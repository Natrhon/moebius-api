<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
Route::get('reset_password/{token}', ['as' => 'password.reset', function($token)
{
    // implement your reset password route here!
}]);

Route::get('/', function () {
    return view('welcome');
});

Route::get('/user', function() {
		

	// inizializzo curl
	$ch = curl_init();

	// imposto la URl del form remoto
	curl_setopt($ch, CURLOPT_URL, 'http://www.swcombine.com/ws/v1.0/character/?access_token=81bddda24831d69cc39bf03420523359');
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Authorization: OAuth 81bddda24831d69cc39bf03420523359' 
	));

	// preparo l'invio dei dati tramite il metodo POST
	//curl_setopt($ch, CURLOPT_POST, true);
	//curl_setopt($ch, CURLOPT_POSTFIELDS, $dati);
	//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// eseguo la chiamata
	curl_exec($ch);
	//var_dump($output);
	// chiudo
	curl_close($ch);
	
});

Route::get('/swchook', function() {

	
	
	$dati=Request::all();
	Log::info('Accesso a swchook. '.json_encode($dati));
	// Create a client with a base URI
	$client = new \GuzzleHttp\Client();
    $response = $client->request('POST', 'http://www.swcombine.com/ws/oauth2/token/', [
        'form_params' => [
			'code' => $dati['code'],
			'client_id' => '901b2898c80418f2111f6b5fa45e383f20570099',
			'client_secret' => '7b7179e06ecd08345fdfd7344181fb1456b11b0e',
			'redirect_uri' => 'http://www.swc-moebius.top/swchook',
			'grant_type' => 'authorization_code',
			'access_type' => 'offline'
        ]
    ]);
    $response = $response->getBody()->getContents();
	 
	$XmlToArray = new Mtownsend\XmlToArray\XmlToArray;
	$response = $XmlToArray ->convert($response);
	
	$getUser = $client->request('GET', 'http://www.swcombine.com/ws/v1.0/character/?access_token='.$response['access_token']);
    $getUser = $getUser->getBody()->getContents();
	$getUser = $XmlToArray ->convert($getUser);
	
	

	Log::info('BODY:'.$response['access_token'].' '.json_encode($getUser));
	return 'eccolo swchook due!<br>'.$dati['code'].'</br>'.json_encode($response).'</br>'.json_encode($getUser);  

});
